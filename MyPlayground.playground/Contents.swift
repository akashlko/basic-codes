import UIKit
import Foundation



/*

var employee : [Double] = [45000, 36000, 32000, 80000]
print(employee.count)
employee.append(39000.34)
print(employee.count)
employee.remove(at: 1)
print(employee.count)
 


var students = [String]()
print(students.count)
students.append("Akash")
students.append("Sneh")
students.append("Singh")
students.append("Bhat")
students.append("Sky")
students.remove(at: 4)
print(students)
*/


/* - Arrays

var salaries = [45000, 10000, 35000, 55000]
var index=0
repeat {
    salaries[index] = salaries[index] + (salaries[index] * Int(0.10))
    index += 1
} while (index < salaries.count)
for index in 0..<salaries.count {
    salaries[index] = salaries[index] + (salaries[index] * Int(0.10))
    print(salaries[index])
}


for i in 1..<5{
    print("index : \(i)")
}



for index in salaries{
    print("salary: \(index)")
}

*/





/* - Dictionary

var airports: [String: String] = ["XYZ": "Toronto Pearson", "LAX": "Los Angeles International"]
print("The airport dictionary has: \(airports.count) items")
airports["LHR"] = "London"
airports["DEL"] = "Delhi"
for key in airports.keys{
    print("key: \(key)")
}
for val in airports.values{
    print("Values: \(val)")
}

*/



/* - Optionals

struct person {
    let firstName : String
    let middleName : String?
    let lastName : String
    func printFullName(){
    let middle = middleName ?? ""
        print("\(firstName) \(middle) \(lastName)")
    }
}
var person1 = person(firstName: "Akash", middleName: nil, lastName: "Singh")
var person2 = person(firstName: "Sneh", middleName: nil, lastName: "Bhat")
person1.printFullName()
person2.printFullName()



-------------------------------------------------------------------------------------------------

 

struct Person {
    let firstName : String
    let middleName : String?
    let lastName : String
    let spouse : String?
    init(firstName: String, middleName: String?, lastName: String, spouse: String?){
        self.firstName = firstName
        self.middleName = middleName
        self.lastName = lastName
        self.spouse = spouse
    }
    func printFullName() -> String{
        let middle = middleName ?? " "
        return "\(firstName) \(middle) \(lastName)"
    }
}

let person = Person(firstName: "Akash", middleName: "Pratap", lastName: "Singh", spouse: nil)
if let spouseName = person.spouse?.getFullName(){
    print(spouseName)
} else {
    print("\(person.firstName) does not have a spouse")
}

*/



/* -OOPs : Class & Object

class Vehicle {
 
    //Class Properties
    var tires = 4
    var headlights = 2
    var horsepower = 468
    var model = " "

    //Functions- That can do stuff

    func drive() {

    }

    func brake() {

    }

}
 

//Ability to be instantiated

let bmw = Vehicle()
bmw.model = "328i"
let ford = Vehicle()
ford.model = "f350"
ford.brake()
func passByReference(vehicle: Vehicle) {
    vehicle.model = "Cheese"
}
print(ford.model)
passByReference(vehicle: ford)
print(ford.model)


// -OOPs : Inheritance

class Vehicle {
    var tires = 4
    var make : String?
    var model : String?
    var currentSpeed : Double = 0
    init() {
        print("I AM THE PARENT")
    }
    func drive(speedIncrease: Double) {
        currentSpeed += speedIncrease * 2
    }
    func brake(){
    }
}



class truck: Vehicle {
    override init() {
        super.init()
    }
    override func drive(speedIncrease: Double) {
        currentSpeed += speedIncrease
    }
}

class sportsCar: Vehicle {
    override init() {
        super.init()
        print("I AM THE CHILD")
        make = "BMW"
        model = "3 Series"
    }
    override func drive(speedIncrease: Double) {
        currentSpeed += speedIncrease * 3
    }
}
let car = sportsCar()

 
// -OOPs : Polymorphism
 
class Shape {
    var area: Double?
    func calculateArea(valA: Double, valB: Double){
 
    }
}
class Triangle: Shape {
    override func calculateArea(valA: Double, valB: Double) {
        area = (valA * valB)/2
    }
}

class Rectangle: Shape {
    override func calculateArea(valA: Double, valB: Double) {
        area = valA * valB
    }
}

*/

/* -Enumerations



//Basic Syntax

enum NameOfEnum {
    case caseOne, caseTwo, caseThree
}
let enumeration: NameOfEnum = .caseTwo
 
enum Barcode {
    case upc(Int, Int, Int, Int)
    case qrCode(String)
}

var productBarcode = Barcode.upc(8, 85909, 51226, 3)
productBarcode = .qrCode("snjdsfjdsfjndvjndsjvnd")
 
switch productBarcode{
 
case let .upc(numberSystem, manufacturer, productCode, check) :
    print("UPC \(numberSystem), \(manufacturer), \(productCode), \(check)")
 
case let .qrCode(productCode) :
    print("QR CODE: \(productCode)")

}
 
enum JediMaster: String {
    case yoda = "Master Yoda"
    case macewindu = "Mace windu"
    case quigonJim = "Qui-Gon Jinn"
    case obiWanKenobi = "Obi-wan Kenobi"
    case lukeSkyWalker = "Luke Skywalker"
}
print(JediMaster.yoda.rawValue)
 
 
enum SwitchStatus {
    case on
    case off
}
var switchStatus: SwitchStatus = .off
func flipSwitch(status: SwitchStatus) -> SwitchStatus{
    if status == .off {
        return .on
    } else {
        return .off
    }
}
flipSwitch(status: switchStatus)
switchStatus = .on
flipSwitch(status: switchStatus)
*/



// -Extensions : They allow you to add functionality to an existing class, structure, enumeration or even a protocol.

//Syntax -

/*
class MyClass {

}
extension MyClass {

    //functionality

}



extension String{
    func reverse()-> String{
        var characterArray = [Character]()
        for character in self.characters {
            characterArray.insert(character, at: 0)
        }
        return String(characterArray)
    }
}

var name = "Akash Singh"
name.reverse()

extension Int{
    func square() -> Int {
        return self * self
    }
}
var value = 9
value.square()
print(value)



//Mutating function : It allows that function to modify the value of whatever variable that is called upon.
 
 
extension Double{
    mutating func calculateArea(){
        let pi = 3.1415
        self = pi * (self * self)
    }
}

class Circle{
    var radius: Double
    init(radius: Double){
        self.radius = radius
    }
}
var circle = Circle(radius: 3.3)
print(circle.radius)
circle.radius.calculateArea()
print(circle.radius)
*/



// Protocols - Protocol defines a blueprint of methods or properties that can then be adopted by classes (or any other types).
/*
protocol Number {
    var floatValue: Float { get }
}
extension Float: Number {
    var floatValue: Float{
        return self
    }
}
let possibleString : String? = "An unwrapped optional"
let forcedString : String = possibleString!
let assumedString : String! = "An implicitly unwrapped optional string"
let implicitString : String = assumedString
var greeting =  "Akash Singh "
greeting.count
greeting[greeting.index(after: greeting.startIndex)]
greeting[greeting.index(before: greeting.endIndex)]
for index in greeting.indices{
        print("\(greeting[index])", terminator:" ")
}
greeting.insert(contentsOf: " Lucknow", at: greeting.index(before: greeting.endIndex))
print(greeting)
greeting.remove(at: greeting.index(before: greeting.endIndex))
print(greeting)
var favouriteGenres: Set = ["Rock", "Classical", "Hip-Hop"]
var favouriteArts: Array = ["Rock", "Classical", "Hip-Hop"]
favouriteArts.sorted()
print(favouriteGenres)
print(favouriteArts)


let Arr: [String] = ["akash"]
print(Arr)

let arr : Set<String> = ["akash","akash","sneh"]
print(arr)


let arr1 : [String:Int] = ["First":450, "Second":400]
print(arr1)

*/

/*
//Optionals
let Name : String? = "Akash"
print(Name)

//Optional Binding
if let userName = Name{
    print(userName)
}

//Forced Unwrapping
let userName = Name!
print(userName)

//Implicitly Unwrapped Optionals
var name: String! = "Akash Singh"
print(name)

//Tuples
let student : (userID: Int, name: String, isGood: Bool) = (1296, "Akash Singh", true)
print(student.userID)
print(student.name)
print(student.isGood)
*/

/*
//Functions ->
func sayHello(){
    print("Hello world")
}
sayHello()

func add(a: Int, b: Int){
    print(a+b)
}
add(a: 3, b: 4)

func multiply(a: Int, b: Int) -> Int{
   let result = a*b
    return result
}
let result = multiply(a: 5, b: 6)
print(result)
 

func isGoodBoy(totalMarks :  Int) -> (isGoodBoy: Bool, message: String){
    if totalMarks >= 450{
        return (true, "very good boy")
    } else{
        return(false, "need improvement")
    }
}

let  status  = isGoodBoy(totalMarks: 450)
print(status.isGoodBoy)
print(status.message)

 */

//Function with Optional Return type
func getUserIdForUsername(userName: String) -> String?{
    switch(userName){
    case "PJ":
            return  "12345"
    case "JP":
            return  "67890"
    default:
        return nil
    }
}

//Function argument labels and parameter names
func divide(Argument a: Int, by b: Int) ->  Int{
    return a/b
}
let result1 = divide(Argument: 6, by: 3)
print("division of a and b gives \(result1)")

